<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\impl;

use lflow\lib\interface\Expression;
use RulerZ\RulerZ;
use RulerZ\Compiler\Compiler;
use RulerZ\Target\Native\Native;

/**
 * 表达式引擎实现
 *
 * @author Mr.April
 * @since  1.0
 */
class JuelExpression implements Expression
{

    /**
     * @param string $expr
     * @param object $args 参数
     *
     *
     * @return bool
     */
    public function eval(string $expr, object $args): bool
    {
        try {
            // 创建一个RulerZ编译器
            $compiler = Compiler::create();

            // 创建RulerZ实例
            $rulerZ = new RulerZ($compiler, [new Native(['length' => 'strlen'])]);

            // 应用规则到对象
            $result = $rulerZ->satisfies($args, $expr);
            return $result ?? false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
