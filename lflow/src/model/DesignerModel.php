<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\model;

use lflow\lib\util\Str;
use think\db\Query;
use think\model\concern\SoftDelete;

class DesignerModel extends BaseModel
{

    use SoftDelete;

    protected string $deleteTime = 'delete_time';

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     *
     * @var string
     */
    protected $name = 'wf_model_designer';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'update_time' => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * JSON字段
     *
     * @var string[]
     */
    protected $json = ['variable'];

    /**
     * 创建字符串id
     *
     * @param $model
     *
     * @return void
     */
    protected static function onBeforeInsert($model): void
    {
        $uuid                = !empty($model->{$model->pk}) ? $model->{$model->pk} : Str::uuid();
        $model->{$model->pk} = $uuid;
    }

    /**
     * ID搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('id', $value);
        }
    }

    /**
     * 分组ID搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchModelGroupIdAttr(Query $query, $value)
    {
        if ($value != '') {
            $query->where('model_group_id', $value);
        }
    }

    /**
     * 模型类型搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchModelTypeAttr(Query $query, $value)
    {
        if ($value != '') {
            $query->where('model_type', $value);
        }
    }

    /**
     * 模型编码搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchModelKeyAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('model_key', $value . '%');
        }
    }

    /**
     * 模型名称搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchModelNameAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('model_name', $value . '%');
        }
    }

    /**
     * 模型用户ID搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchUserIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('user_id', $value);
        }
    }

}
