<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\model;

use lflow\lib\util\entity\Task;
use phoenix\basic\BaseModel;
use phoenix\utils\Str;
use think\db\Query;

class CcOrderModel extends BaseModel
{

    /**
     * 模型名称
     *
     * @var string
     */
    protected $name = 'wf_cc_order';

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'finish_time';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'finish_time' => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * 新增自动创建字符串id
     *
     * @param $model
     *
     * @return void
     */
    protected static function onBeforeInsert($model): void
    {
        $uuid                = !empty($model->{$model->pk}) ? $model->{$model->pk} : Str::uuid();
        $model->{$model->pk} = $uuid;
    }

    /**
     * OrderId搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchOrderIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.order_id';
            $query->where($field, $value);
        }
    }

}
