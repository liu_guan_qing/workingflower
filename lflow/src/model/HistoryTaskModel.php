<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\model;

use lflow\lib\util\Str;
use think\db\Query;

class HistoryTaskModel extends BaseModel
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     *
     * @var string
     */
    protected $name = 'wf_hist_task';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'finish_time';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'finish_time' => 'timestamp:Y-m-d H:i:s',
        'expire_time' => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * JSON字段
     *
     * @var string[]
     */
    protected $json = ['variable'];

    public array $actor_ids = [];

    /**
     * 新增自动创建字符串id
     *
     * @param $model
     *
     * @return void
     */
    protected static function onBeforeInsert($model): void
    {
        $uuid                = !empty($model->{$model->pk}) ? $model->{$model->pk} : Str::uuid();
        $model->{$model->pk} = $uuid;
    }

    /**
     * id搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.id';
            $query->where($field, $value);
        }
    }

    /**
     * ProcessId搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchProcessIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.process_id';
            $query->where($field, $value);
        }
    }

    /**
     * OrderId搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchOrderIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.order_id';
            $query->where($field, $value);
        }
    }

    /**
     * TaskName 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchTaskNameAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.task_name';
            $query->where($field, $value);
        }
    }

    /**
     * TaskState 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchTaskStateAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.task_state';
            $query->where($field, $value);
        }
    }

    /**
     * PerformType 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchPerformTypeAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.perform_type';
            $query->where($field, $value);
        }
    }

    /**
     * Operator 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchOperatorAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.operator';
            $query->where($field, $value);
        }
    }

    /**
     * Version 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchVersionAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.version';
            $query->where($field, $value);
        }
    }

    public function searchParentTaskIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.parent_task_id';
            $query->where($field, $value);
        }
    }

    /**
     * 定义与TaskActorModel模型的关联关系
     *
     * @return \think\model\relation\BelongsTo
     */
    public function taskActor(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(HistoryTaskActorModel::class, 'id', 'task_id')->bind(['actor_id']);
    }

    public function searchActorIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->where('taskActor.actor_id', $value);
        }
    }

    /**
     * 定义与OrderModel模型的关联关系
     *
     * @return \think\model\relation\BelongsTo
     */
    public function orders(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(HistoryOrderModel::class, 'order_id', 'id');
    }

    /**
     * 定义与ProcessModel模型关联关系
     *
     * @return \think\model\relation\BelongsTo
     */
    public function processs(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(ProcessModel::class, 'process_id', 'id');
    }

    /**
     * 定义ProcessModel DisplayName搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchDisplayNameAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $query->whereLike('processs.display_name', '%' . $value . '%');
        }
    }

}
