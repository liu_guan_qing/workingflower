<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\model;

use lflow\lib\util\entity\Task;
use phoenix\basic\BaseModel;
use phoenix\utils\Str;
use think\db\Query;

class HistoryOrderModel extends BaseModel
{

    /**
     * 数据表主键
     *
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     *
     * @var string
     */
    protected $name = 'wf_hist_order';

    // 定义时间戳字段名
    protected $createTime = 'create_time';
    protected $updateTime = 'end_time';

    protected $type = [
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'end_time'    => 'timestamp:Y-m-d H:i:s',
        'expire_time' => 'timestamp:Y-m-d H:i:s',
    ];

    /**
     * JSON字段
     *
     * @var string[]
     */
    protected $json = ['variable'];

    /**
     * 新增自动创建字符串id
     *
     * @param $model
     *
     * @return void
     */
    protected static function onBeforeInsert($model): void
    {
        $uuid                = !empty($model->{$model->pk}) ? $model->{$model->pk} : Str::uuid();
        $model->{$model->pk} = $uuid;
    }

    /**
     * Id 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.id';
            $query->where($field, $value);
        }
    }

    /**
     * ProcessId搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchProcessIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.process_id';
            $query->where($field, $value);
        }
    }

    /**
     * OrderState搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchOrderStateAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.order_state';
            $query->where($field, $value);
        }
    }

    /**
     * Creator搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchCreatorAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.creator';
            $query->where($field, $value);
        }
    }

    /**
     * Priority 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchPriorityAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.priority';
            $query->where($field, $value);
        }
    }

    /**
     * ParentId 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchParentIdAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.parent_id';
            $query->where($field, $value);
        }
    }

    /**
     * OrderNo 搜索器
     *
     * @param \think\db\Query $query
     * @param                 $value
     */
    public function searchOrderNoAttr(Query $query, $value)
    {
        if (!empty($value)) {
            $field = $query->getTable() . '.order_no';
            $query->where($field, $value);
        }
    }

    /**
     * 模型关联定义-流程定义
     *
     * @return \think\model\relation\BelongsTo
     */
    public function processs(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(ProcessModel::class, 'process_id', 'id');
    }

}
