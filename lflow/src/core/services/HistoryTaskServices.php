<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\core\services;

use app\adminapi\controller\v1\wf\Task;
use lflow\core\BaseServices;
use lflow\dao\HistoryTaskDao;
use lflow\model\TaskModel;

class HistoryTaskServices extends BaseServices
{

    /**
     * @param \lflow\dao\HistoryTaskDao $dao
     */
    public function __construct(HistoryTaskDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 历史记录还原TaskModel
     *
     * @param $self
     *
     * @return \lflow\model\TaskModel
     */
    public function undoTask($self): TaskModel
    {
        $task = new TaskModel();
        $task->set('order_id', $self->getData('order_id'));
        $task->set('task_name', $self->getData('task_name'));
        $task->set('display_name', $self->getData('display_name'));
        $task->set('task_type', $self->getData('task_type'));
        $task->set('expire_time', $self->getData('expire_time'));
        $task->set('action_url', $self->getData('action_url'));
        $task->set('parent_task_id', $self->getData('parent_task_id'));
        $task->set('variable', $self->getData('variable'));
        $task->set('perform_type', $self->getData('perform_type'));
        $task->set('operator', $self->getData('operator'));
        return $task;
    }

}
