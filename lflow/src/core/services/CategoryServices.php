<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\core\services;

use lflow\core\BaseServices;
use lflow\dao\CategoryDao;
use lflow\exceptions\WorkFlowException;

class CategoryServices extends BaseServices
{

    /**
     * @param \lflow\dao\CategoryDao $dao
     */
    public function __construct(CategoryDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 保存资源
     *
     * @param array $data
     *
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function save(array $data): mixed
    {
        if ($this->dao->getOne(['name' => $data['name']])) {
            throw new WorkFlowException('该分类已存在');
        }
        $res = $this->dao->save($data);
        if (!$res) throw new WorkFlowException('保存失败');
        return $res;
    }

    /**
     * 保存修改资源
     *
     * @param string $id
     * @param array  $data
     *
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(string $id, array $data): mixed
    {
        $result = $this->dao->getOne(['id' => $id]);
        if ($result && $result['id'] != $id) {
            throw new WorkFlowException('该分类已存在');
        }
        return $this->dao->update($id, $data);
    }

    /**
     * 删除分类
     *
     * @param string $id
     *
     * @return mixed
     */
    public function del(string $id): mixed
    {
        $modelDesignerServices = app()->make(ModelDesignerServices::class);
        $count                 = $modelDesignerServices->count(['model_group_id' => $id]);
        if ($count) {
            throw new WorkFlowException('请先删除分类下的模型');
        } else {
            return $this->dao->delete($id);
        }
    }

    /**
     * 批量删除
     *
     * @param string $ids
     *
     * @return bool
     */
    public function batchRemove(string $ids): bool
    {
        $modelDesignerServices = app()->make(ModelDesignerServices::class);
        foreach (explode(',', $ids) as $value) {
            $count = $modelDesignerServices->count(['model_group_id' => $value]);
            if ($count) {
                throw new WorkFlowException('请先删除分类下的模型');
            }
        }
        return $this->dao->batchDelete(explode(',', $ids));
    }

    /**
     * 分组列表
     *
     * @param array $where
     *
     * @return array
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function select(array $where): array
    {
        [$page, $limit] = $this->getPageValue();
        $list  = $this->dao->selectList($where, '*', $page, $limit, 'name', [], true);
        $count = $this->dao->count($where);
        return compact('list', 'count');
    }

    /**
     * 查询指定数据
     *
     * @param string $id
     *
     * @return array|\think\Model|null
     */
    public function find(string $id): array|\think\Model|null
    {
        return $this->get($id, ['*']);
    }

}
