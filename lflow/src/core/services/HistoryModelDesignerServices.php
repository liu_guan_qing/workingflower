<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\core\services;

use lflow\core\BaseServices;
use lflow\dao\TaskDao;

class HistoryModelDesignerServices extends BaseServices
{

    /**
     * @param \lflow\dao\TaskDao $dao
     */
    public function __construct(TaskDao $dao)
    {
        $this->dao = $dao;
    }

}
