<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

/**
 * 工作元素
 *
 * @author Mr.April
 * @since  1.0
 */
abstract class WorkFlowCkpt extends NodeCkpt
{
    /**
     * @var string
     */
    private string $form;

    public function getForm(): string
    {
        return $this->form ?? '';
    }

    public function setForm(string $form): void
    {
        $this->form = $form;
    }
}
