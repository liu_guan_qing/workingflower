<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;


use lflow\core\Execution;
use lflow\handlers\impl\CreateTaskHandler;
use lflow\handlers\impl\StartSubProcessHandler;
use lflow\lib\util\Logger;

/**
 * 变迁元素Transition元素
 *
 * @author Mr.April
 * @since  1.0
 */
class TransitionCkpt extends BaseCkpt
{
    /**
     * 变迁的源节点引用
     */
    private NodeCkpt $source;

    /**
     * 变迁的目标节点引用
     */
    private NodeCkpt $target;

    /**
     * 变迁的目标节点name名称
     */
    private string $to;

    /**
     * 变迁的条件表达式，用于decision
     */
    private string|null $expr;

    /**
     * 转折点图形数据
     */
    private string $g;

    /**
     * 描述便宜位置
     */
    private string $offset;

    /**
     * 当前变迁路径是否可用
     */
    private bool $enabled = false;


    public function execute(Execution $execution)
    {

        if (!$this->isEnabled()) return;
        if ($this->getTarget() instanceof TaskCkpt) {
            //如果目标节点模型为TaskModel，则创建task
            $this->fire(new CreateTaskHandler($this->getTarget()), $execution);
        } else if ($this->getTarget() instanceof SubProcessCkpt) {
            //如果目标节点模型为SubProcessModel，则启动子流程
            $this->fire(new StartSubProcessHandler($this->getTarget()), $execution);
        } else {
            //如果目标节点模型为其它控制类型，则继续由目标节点执行
            $this->getTarget()->execute($execution);
        }
    }

    public function getSource(): NodeCkpt
    {
        return $this->source;
    }

    public function setSource(NodeCkpt $source): void
    {
        $this->source = $source;
    }

    public function getTarget(): NodeCkpt
    {
        return $this->target;
    }

    public function setTarget(NodeCkpt $target): void
    {
        $this->target = $target;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getExpr(): string|null
    {
        return $this->expr;
    }

    public function setExpr(string|null $expr): void
    {
        $this->expr = $expr;
    }

    public function getG(): string
    {
        return $this->g;
    }

    public function setG(string $g): void
    {
        $this->g = $g;
    }

    public function getOffset(): string
    {
        return $this->offset;
    }

    public function setOffset(string $offset): void
    {
        $this->offset = $offset;
    }

}
