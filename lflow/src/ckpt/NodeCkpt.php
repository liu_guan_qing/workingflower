<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\ckpt;

use lflow\core\Execution;
use lflow\exceptions\WorkFlowException;
use lflow\lib\util\AssertHelper;
use lflow\lib\util\ClassUtil;
use lflow\lib\util\Logger;
use lflow\lib\util\StringHelper;
use ReflectionClass;

/**
 * 节点元素（存在输入输出的变迁）
 *
 * @author Mr.April
 * @since  1.0
 */
abstract class NodeCkpt extends BaseCkpt
{

    /**
     * 输入变迁集合
     */
    public array $inputs = [];

    /**
     * 输出变迁集合
     */

    public array $outputs = [];

    /**
     * 局部前置拦截器
     */
    private string $preInterceptors;

    /**
     * 局部后置拦截器
     */
    private string $postInterceptors;

    /**
     * 前置局部拦截器实例集合
     */
    private array $preInterceptorList = [];

    /**
     * 后置局部拦截器实例集合
     */
    private array $postInterceptorList = [];

    /**
     * 具体节点模型需要完成的执行逻辑
     *
     * @param \lflow\core\Execution $execution
     *
     * @return void
     */
    abstract public function exec(Execution $execution): void;

    /**
     * 对执行逻辑增加前置、后置拦截处理
     *
     * @param \lflow\core\Execution $execution
     *
     * @return void
     */
    public function execute(Execution $execution): void
    {
        $this->intercept($this->preInterceptorList, $execution);
        $this->exec($execution);
        $this->intercept($this->postInterceptorList, $execution);
    }

    /**
     * 运行变迁继续执行
     *
     * @param \lflow\core\Execution $execution 执行对象
     */
    public function runOutTransition(Execution $execution): void
    {
        foreach ($this->getOutputs() as $Transition) {
            $Transition->setEnabled(true);
            $Transition->execute($execution);
        }
    }

    /**
     * 拦截方法
     *
     * @param array                 $interceptorList 拦截器列表
     * @param \lflow\core\Execution $execution       执行对象
     *
     * @return void
     */
    public function intercept(array $interceptorList, Execution $execution): void
    {
        try {
            foreach ($interceptorList as $interceptor) {
                $interceptor->intercept($execution);
            }
        } catch (WorkFlowException $e) {
            Logger:: error("拦截器执行失败=" . json_encode($e->getMessage()));
            throw new WorkFlowException($e->getMessage());
        }
    }

    /**
     * * 根据父节点模型、当前节点模型判断是否可退回。可退回条件：
     * 1、满足中间无fork、join、subprocess模型
     * 2、满足父节点模型如果为任务模型时，参与类型为any
     *
     * @param \lflow\ckpt\NodeCkpt $current
     * @param \lflow\ckpt\NodeCkpt $parent
     *
     * @return bool
     */
    public function canRejected(NodeCkpt $current, NodeCkpt $parent): bool
    {
        if ($parent instanceof TaskCkpt && !$parent->isPerformAny()) {
            return false;
        }
        $result = false;
        foreach ($current->getInputs() as $tm) {
            $source = $tm->getSource();
            if ($source == $parent) {
                return true;
            }

            if ($source instanceof ForkCkpt || $source instanceof JoinCkpt || $source instanceof SubProcessCkpt || $source instanceof StartCkpt) {
                continue;
            }
            $result = $result || $this->canRejected($source, $parent);
        }
        return $result;
    }

    public function getNextCkpts(object $clazz): array
    {
        $temp   = [];
        $models = [];
        foreach ($this->getOutputs() as $tm) {
            $this->addNextCkpts($models, $tm, $clazz, $temp);
        }
        return $models;
    }

    protected function addNextCkpts(&$models, $tm, $clazz, $temp): void
    {
        if (count($temp) != 0 && in_array($tm->getTo(), array_keys($temp))) {
            return;
        }
        $obj = new ReflectionClass($clazz);
        if ($obj->isInstance($tm->getTarget())) {
            $models[] = $tm->getTarget();
        } else {
            foreach ($tm->getTarget()->getOutputs() as $tm2) {
                $temp[$tm->getTo()] = $tm->getTarget();
                $this->addNextCkpts($models, $tm2, $clazz, $temp);
            }
        }
    }

    public function setInputs($inputs): void
    {
        $this->inputs = $inputs;
    }

    public function getInputs(): array
    {
        return $this->inputs;
    }

    public function setOutputs(array $outputs): void
    {
        $this->outputs = $outputs;
    }

    public function getOutputs(): array
    {
        return $this->outputs ?? [];
    }

    public function getPreInterceptors(): string
    {
        return $this->preInterceptors;
    }

    public function setPreInterceptors(string|null $preInterceptors): void
    {
        $classZ                = str_replace('/', '\\', !empty($preInterceptors) ? $preInterceptors : '');
        $this->preInterceptors = $classZ;
        if (StringHelper::isNotEmpty($classZ)) {
            foreach (explode(',', $classZ) as $interceptor) {
                $interceptors = ClassUtil::instantiateClass($interceptor);
                AssertHelper::notNull($interceptors, $interceptor . 'class 前置拦截器不存在');
                if (!empty($interceptor)) {
                    $this->preInterceptorList[] = $interceptors;
                }
            }
        }
    }

    public function getPostInterceptors(): string
    {
        return $this->postInterceptors;
    }

    public function setPostInterceptors(string|null $postInterceptors): void
    {
        $classZ                 = str_replace('/', '\\', !empty($postInterceptors) ? $postInterceptors : '');
        $this->postInterceptors = $classZ;
        if (StringHelper::isNotEmpty($classZ)) {
            foreach (explode(',', $classZ) as $interceptor) {
                $interceptors = ClassUtil::instantiateClass($interceptor);
                AssertHelper::notNull($interceptors, $interceptor . 'class 后置拦截器不存在');
                if (!empty($interceptor)) {
                    $this->postInterceptorList[] = $interceptors;
                }
            }
        }
    }
}
