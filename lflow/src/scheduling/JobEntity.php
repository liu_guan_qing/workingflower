<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\scheduling;

use lflow\entity\Task;

class JobEntity
{
    /**
     * job主键
     */
    private string $key;
    /**
     * job组
     */
    private string $group;
    /**
     * 任务对应的业务id串
     */
    private string $id;
    /**
     * 节点模型名称
     */
    private string $modelName;

    /**
     * job类型
     */
    private int $jobType;
    /**
     * 任务对象
     */
    private Task $task;
    /**
     * 启动时间
     */
    private string|int $startTime;

    /**
     * 间隔时间(分钟)
     */
    private int $period;

    /**
     * 执行参数
     */
    private string $args;

    public function __construct(string $id, Task $task, string|int $startTime, string $args, int $period = 0)
    {
        $this->id        = $id;
        $this->task      = $task;
        $this->startTime = $startTime;
        $this->period    = $period;
        $this->args      = $args;
    }

    public function getTask(): Task
    {
        return $this->task;
    }

    public function setTask(Task $task): void
    {
        $this->task = $task;
    }

    public function getStartTime(): string
    {
        return $this->startTime;
    }

    public function setStartTime(string $startTime): void
    {
        $this->startTime = $startTime;
    }

    public function getPeriod(): int
    {
        return $this->period;
    }

    public function setPeriod(int $period): void
    {
        $this->period = $period;
    }

    public function getArgs(): string
    {
        return $this->args;
    }

    public function setArgs(string $args)
    {
        $this->args = $args;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getModelName(): string
    {
        return $this->modelName;
    }

    public function setModelName(string $modelName): void
    {
        $this->modelName = $modelName;
    }

    public function getJobType(): string|int
    {
        return $this->jobType;
    }

    public function setJobType(int $jobType): void
    {
        $this->jobType = $jobType;
    }

    public function getKey(): string
    {
        return $this->key ?? '';
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function getGroup(): string
    {
        return $this->group??'';
    }

    public function setGroup(string $group)
    {
        $this->group = $group;
    }

}