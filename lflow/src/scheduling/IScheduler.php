<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\scheduling;

/**
 * 调度器接口，与具体的定时调度框架无关
 *
 * @author GQL
 * @since  1.0
 */
interface IScheduler
{
    const CONFIG_REPEAT = "scheduler.repeat";
    const CONFIG_USECALENDAR = "scheduler.useCalendar";
    const CONFIG_HOLIDAYS = "scheduler.holidays";
    const CONFIG_WEEKS = "scheduler.weeks";
    const CONFIG_WORKTIME = "scheduler.workTime";
    const CALENDAR_NAME = "snakerCalendar";

    const KEY = "id";
    const MODEL = "model";
    const GROUP = "snaker";
    const TYPE_EXECUTOR = "executor.";
    const TYPE_REMINDER = "reminder.";

    /**
     * 调度执行方法
     *
     * @param \lflow\scheduling\JobEntity $entity
     */
    public function schedule(JobEntity $entity): void;

    /**
     * 停止调度
     *
     * @param string $key
     *
     * @return mixed
     */
    public function delete(string $key);
}
