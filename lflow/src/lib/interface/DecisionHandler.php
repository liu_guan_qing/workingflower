<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\interface;

use lflow\core\Execution;

/**
 * 决策处理器接口
 *
 * @author Mr.April
 * @since  1.0
 */
interface DecisionHandler
{
    /**
     * 定义决策方法，实现类需要根据执行对象做处理，并返回后置流转的name
     *
     * @param Execution $execution
     *
     * @return string 后置流转的name
     */
    public function decide(Execution $execution): string;
}
