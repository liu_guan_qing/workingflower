<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\interface;

/**
 * 表达式引擎实现
 *
 * @author Mr.April
 * @since  1.0
 */
interface Expression
{
    /**
     *
     * @param string $expr 表达式
     * @param object $args 参数
     *
     * @return bool
     */
    public function eval(string $expr, object $args): bool;

}
