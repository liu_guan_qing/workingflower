<?php

namespace lflow\lib\interface;

use lflow\model\HistoryOrderModel;
use lflow\model\HistoryTaskModel;

/**
 * 任务、实例完成时触发动作的接口
 *
 * @author Mr.April
 * @since  1.0
 */
interface Completion
{
    /**
     * 任务完成触发执行||实例完成触发执行
     *
     * @param \lflow\model\HistoryTaskModel|\lflow\model\HistoryOrderModel $model
     */
    public function complete(HistoryTaskModel|HistoryOrderModel $model): void;

}
