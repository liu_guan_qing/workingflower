<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\trait;



use lflow\core\QueryServices;
use lflow\lib\util\AssertHelper;

trait ClassService
{
    public function process()
    {
        AssertHelper::notNull($this->processService);
        return $this->processService;
    }

    public function order()
    {
        AssertHelper::notNull($this->orderService);
        return $this->orderService;
    }

    public function task()
    {
        AssertHelper::notNull($this->taskService);
        return $this->taskService;
    }

    public function query():QueryServices
    {
        AssertHelper::notNull($this->queryService);
        return $this->queryService;
    }

    public function manager()
    {
        AssertHelper::notNull($this->managerService);
        return $this->managerService;
    }
}
