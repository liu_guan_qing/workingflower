<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\util;



use lflow\exceptions\WorkFlowException;

/**
 * 断言帮助类
 *
 * @author Mr.April
 * @since  1.0
 */
abstract class AssertHelper
{

    /**
     * 断言表达式为true
     *
     * @param bool   $expression
     * @param string $message 异常打印信息
     *
     * @throws lflow\exceptions\WorkFlowException
     */
    public static function isTrue(bool $expression, string $message = "[Assertion failed] - this expression must be true"): void
    {
        if (!$expression) {
            throw new WorkFlowException($message);
        }
    }

    /**
     * 断言给定的object对象为空
     *
     * @param \helper\mixed $object |null $object
     * @param string        $message
     *
     * @throws lflow\exceptions\WorkFlowException
     */
    public static function isNull(mixed $object, string $message = "[Assertion failed] - the object argument must be null"): void
    {
        if (!empty($object)) {
            throw new WorkFlowException($message);
        }
    }

    /**
     * 断言给定的object对象为非空
     *
     * @param object|string|null $object
     * @param string             $message
     *
     * @throws lflow\exceptions\WorkFlowException
     */
    public static function notNull(mixed $object, string $message = "[Assertion failed] - this argument is required; it must not be null"): void
    {
        if (empty($object)) {
            throw new WorkFlowException($message);
        }
    }

    /**
     * 断言给定的字符串为非空
     *
     * @param string|null $str
     * @param string      $message
     *
     * @throws lflow\exceptions\WorkFlowException
     */
    public static function notEmpty(string|null $str, string $message = "[Assertion failed] - this argument is required; it must not be null or empty"): void
    {
        if ($str == null || strlen($str) == 0) {
            throw new WorkFlowException($message);
        }
    }
}
