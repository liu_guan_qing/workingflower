<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\util;

/**
 * Object 处理帮助类
 *
 * @author Mr.April
 * @since  1.0
 */
class ObjectHelper
{
    /**
     * 对象追加
     *
     * @param object $obj1
     * @param object $args
     *
     * @return object
     */
    public static function putAll(object $obj1, object $args): object
    {
        foreach ($obj1 as $key => $value) {
            if (!isset($args->$key)) {
                $args->$key = $value;
            }
        }
        return $args;
    }

    /**
     * PUT对象
     *
     * @param object $object
     * @param string $key
     * @param mixed  $value
     *
     * @return object
     */
    public static function put(object $object, string $key, mixed $value): object
    {
        if (!isset($object->{$key})) {
            $object->{$key} = $value;
        }
        return $object;
    }

    /**
     * Set对象
     * @param object $object
     * @param string $key
     * @param mixed  $value
     *
     * @return object
     */
    public static function set(object $object, string $key, mixed $value): object
    {
        $object->{$key} = $value;
        return $object;
    }

    /**
     * 获取对象对应Value
     *
     * @param object $object
     * @param string $key
     *
     * @return mixed
     */
    public static function getObjectValue(object $object, string $key): mixed
    {
        return $object->{$key} ?? null;
    }
}
