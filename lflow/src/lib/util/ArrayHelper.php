<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\lib\util;

use lflow\lib\util\WorkFlowException;
use ReflectionClass;

/**
 *
 * 数组帮助类
 * @author Mr.April
 * @since  1.0
 */
class ArrayHelper
{
    /**
     * convertHumpToLine
     *
     * @param array $data
     *
     * @return array
     */
    public static function convertHumpToLine(array $data): array
    {
        $result = [];
        foreach ($data as $key => $item) {
            if (is_array($item) || is_object($item)) {
                $result[self::humpToLine($key)] = self::convertHumpToLine((array)$item);
            } else {
                $result[self::humpToLine($key)] = trim($item);
            }
        }
        return $result;
    }

    /**
     * toLineHumpConvert
     *
     * @param array $data
     *
     * @return array
     */
    public static function toLineHumpConvert(array $data): array
    {
        $result = [];
        foreach ($data as $key => $item) {
            if (is_array($item) || is_object($item)) {
                $result[self::humpCamelize($key)] = self::toLineHumpConvert((array)$item);
            } else {
                $result[self::humpCamelize($key)] = trim($item);
            }
        }
        return $result;
    }

    /**
     * humpToLine  字符串大写转下划线
     *
     * @param $str
     *
     * @return array|string|string[]|null
     */
    public static function humpToLine($str): string|null
    {
        $str = preg_replace_callback('/([A-Z]{1})/', function ($matches) {
            return '_' . strtolower($matches[0]);
        }, $str);
        return $str;
    }

    /**
     * humpToLine  字符串大写转下划线
     *
     * @param string $str
     * @param string $separator
     *
     * @return array|string|string[]|null
     */
    public static function humpCamelize(string $str, string $separator = '_'): string|null
    {
        $str = $separator . str_replace($separator, " ", strtolower($str));
        return ltrim(str_replace(" ", "", ucwords($str)), $separator);
    }

    /**
     * 数组转换实体对象
     *
     * @param array  $arr
     * @param string $className
     *
     * @return object
     * @throws \lflow\lib\util\WorkFlowException
     */
    public static function arrayToSimpleObj(array $arr, string $className): object
    {
        try {
            //传递类名或对象进来
            $reflectionClass = new ReflectionClass($className);
            $obj             = $reflectionClass->newInstance();
            foreach ($arr as $key => $value) {
                //过滤验证class 是否包含key
                if ($reflectionClass->hasProperty($key)) {
                    $property = $reflectionClass->getProperty($key);
                    $property->setAccessible(true);//私有属性需要设置允许访问
                    $property->setValue($obj, $value);
                }
            }
            return $obj;
        } catch (\ReflectionException $e) {
            throw new WorkFlowException($e->getMessage());
        }
    }

}
