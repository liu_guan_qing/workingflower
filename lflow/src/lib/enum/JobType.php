<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\enum;

class JobType extends \PhpEnum\Enum
{

    /**
     * 自动处理
     */
    const EXECUTER = [0, "自动处理"];

    /**
     * 消息提醒
     */
    const REMINDER = [1, "消息提醒"];

    const TYPES = [self::EXECUTER,self::REMINDER];

    private $id;
    private $name;

    protected function construct($id, $name)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

}
