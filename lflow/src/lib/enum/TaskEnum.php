<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */

namespace lflow\lib\enum;

class TaskEnum
{

    /**
     * 类型：普通任务
     */
    const PERFORMTYPE_ANY = "ANY";

    /**
     * 类型：参与者fork任务
     */
    const PERFORMTYPE_ALL = "ALL";

    /**
     * 类型：主办任务
     */
    const TASKTYPE_MAJOR = 0;

    /**
     * 类型：协办任务
     */
    const TASKTYPE_AIDANT = 1;

    /**
     * 类型：只作为记录
     */
    const TASKTYPE_RECORD = 2;

    private static array $chineseNames = [
        self::PERFORMTYPE_ANY => "普通任务",
        self::PERFORMTYPE_ALL => "参与者fork任务",
        self::TASKTYPE_MAJOR  => "Major",//主办
        self::TASKTYPE_AIDANT => "Aidant",//协办
        self::TASKTYPE_RECORD => "Record",//记录
    ];

    /**
     * 通过value获取名称
     *
     * @param $value
     *
     * @return string|int
     */
    public static function getName($value): string|int
    {
        return self::$chineseNames[$value] ?? '未知类型';
    }

    /**
     * 通过名称获取value
     *
     * @param $name
     *
     * @return string|int|bool|null
     */
    public static function getValue($name): string|int|bool|null
    {
        $constants = self::getConstants();
        return array_search($name, self::$chineseNames) ?: null;
    }

    /**
     * 通过名称获取ID
     *
     * @param $name
     *
     * @return mixed|null
     */
    public static function getId($name)
    {
        $constants = self::getConstants();

        foreach ($constants as $constantName => $constantValue) {
            if (self::$chineseNames[$constantValue] === $name) {
                return $constantValue;
            }
        }

        return null;
    }

    /**
     * 验证是否包含
     *
     * @param $value
     *
     * @return bool
     */
    public static function isValid($value): bool
    {
        $constants = self::getConstants();

        return in_array($value, $constants);
    }

    /**
     * 获取常量
     *
     * @return array
     */
    private static function getConstants(): array
    {
        $reflectionClass = new \ReflectionClass(self::class);
        return $reflectionClass->getConstants();
    }

    /**
     * 获取声明对应值
     *
     * @return array|string[]
     */
    public static function getChineseNames(): array
    {
        return self::$chineseNames;
    }

}
