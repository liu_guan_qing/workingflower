<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\assign\AssignmentTask;
use lflow\ckpt\FieldCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\ckpt\TaskCkpt;
use lflow\lib\util\ObjectHelper;
use lflow\lib\util\StringHelper;
use lflow\parser\AbstractNodeParser;
use lflow\parser\NodeParser;

/**
 * 任务节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class TaskParser extends AbstractNodeParser
{

    /**
     * 由于任务节点需要解析form、assignee属性，这里覆盖抽象类方法实现
     *
     * @param \lflow\ckpt\NodeCkpt $ckpt
     * @param object|null          $nodes
     */
    protected function parseNode(NodeCkpt $ckpt, ?object $nodes): void
    {
        $properties = $nodes->properties ?? (object)[];
        $properties = is_object($properties) ? $properties : (object)$properties;
        $ckpt->setForm($properties->{NodeParser::ATTR_FORM} ?? '');
        $ckpt->setScope($properties->{NodeParser::ATTR_SCOPE} ?? '');
        $ckpt->setAssignee($properties->{NodeParser::ATTR_ASSIGNEE} ?? '');
        $ckpt->setExpireTime((string)($properties->{NodeParser::ATTR_EXPIRETIME} ?? ''));
        $ckpt->setAutoExecute((string)($properties->{NodeParser::ATTR_AUTOEXECUTE} ?? ''));
        $ckpt->setCallback(ObjectHelper::getObjectValue($properties,NodeParser::ATTR_CALLBACK));
        $ckpt->setReminderTime((string)($properties->{NodeParser::ATTR_REMINDERTIME} ?? ''));
        $ckpt->setReminderRepeat((string)($properties->{NodeParser::ATTR_REMINDERREPEAT} ?? ''));
        $ckpt->setPerformType((string)($properties->{NodeParser::ATTR_PERFORMTYPE} ?? ''));
        $ckpt->setTaskType((string)($properties->{NodeParser::ATTR_TASKTYPE} ?? ''));
        $ckpt->setAssignmentHandler((string)($properties->{NodeParser::ATTR_ASSIGNEE_HANDLER} ?? AssignmentTask::class));//没有使用指定assignmenTask
        $fieldList  = $properties->{NodeParser::ATTR_FIELD} ?? (object)[];
        $fieldCkpt = new FieldCkpt();
        //字段自定义属性
        foreach ($fieldList as $key => $item) {
            $fieldCkpt->setName(StringHelper::equalsIgnoreCase($key, NodeParser::ATTR_NAME) ? $item : '');
            $fieldCkpt->setDisplayName(StringHelper::equalsIgnoreCase($key, NodeParser::ATTR_DISPLAYNAME) ? $item : '');
            $fieldCkpt->setType(StringHelper::equalsIgnoreCase($key, NodeParser::ATTR_TYPE) ? $item : '');
            if ($key === NodeParser::ATTR_ATTR) {
                $attrList = $fieldList->{NodeParser::ATTR_ATTR} ?? (object)[];
                foreach ($attrList as $key1 => $attr) {
                    $fieldCkpt->addAttr($key1, $attr);
                }
            }
        }
        $ckpt->setFields($fieldCkpt);
    }

    /**
     * 产生TaskCkpt模型对象
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new TaskCkpt();
    }
}
