<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\NodeCkpt;
use lflow\ckpt\SubProcessCkpt;
use lflow\lib\util\ConfigHelper;
use lflow\lib\util\ObjectHelper;
use lflow\lib\util\StringHelper;
use lflow\parser\AbstractNodeParser;
use lflow\parser\NodeParser;

/**
 * 子流程节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class WfSubProcessParser extends AbstractNodeParser
{

    /**
     * 由于子流程节点需要解析流程版本等属性，这里覆盖抽象类方法实现
     *
     * @param \lflow\ckpt\NodeCkpt $ckpt
     * @param object|null          $nodes
     */
    protected function parseNode(NodeCkpt $ckpt, ?object $nodes): void
    {
        if ($ckpt instanceof SubProcessCkpt) {
            $properties = ObjectHelper::getObjectValue($nodes, NodeParser::ATTR_PROPERTIES) ?? (object)[];
            $ckpt->setProcessName(ObjectHelper::getObjectValue($properties, NodeParser::ATTR_PROCESSNAME));
            $version = ObjectHelper::getObjectValue($properties, NodeParser::ATTR_VERSION);
            $ver     = 1;
            if (is_numeric($version)) {
                $ver = (int)$version;
            }
            $ckpt->setVersion($ver);
            $form = ObjectHelper::getObjectValue($properties, NodeParser::ATTR_FORM);
            if (StringHelper:: isNotEmpty($form)) {
                $ckpt->setForm($form);
            } else {
                $ckpt->setForm(ConfigHelper::getProperty('subprocessurl'));
            }
        }
    }

    /**
     * 产生SubProcessCkpt模型
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new SubProcessCkpt();
    }
}
