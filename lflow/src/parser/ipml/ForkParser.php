<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\ForkCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\parser\AbstractNodeParser;

/**
 * 分支节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class ForkParser extends AbstractNodeParser
{
    /**
     * 产生ForkModel模型对象
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new ForkCkpt();
    }
}
