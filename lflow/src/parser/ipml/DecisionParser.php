<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\parser\ipml;

use lflow\ckpt\DecisionCkpt;
use lflow\ckpt\NodeCkpt;
use lflow\lib\util\ObjectHelper;
use lflow\parser\AbstractNodeParser;

/**
 * 决策节点解析类
 *
 * @author Mr.April
 * @since  1.0
 */
class DecisionParser extends AbstractNodeParser
{

    /**
     * 产生DecisionModel模型对象
     *
     * @return \lflow\ckpt\NodeCkpt
     */
    protected function newCkpt(): NodeCkpt
    {
        return new DecisionCkpt();
    }

    protected function parseNode(NodeCkpt $ckpt, array|object|null $nodes): void
    {
        $properties = (object)$nodes->properties ?? (object)[];
        $ckpt->setExpr(ObjectHelper::getObjectValue($properties, self::ATTR_EXPR));
        $ckpt->setHandleClass(ObjectHelper::getObjectValue($properties, self::ATTR_HANDLECLASS));
    }
}
