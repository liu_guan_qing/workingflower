<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign\impl;

use lflow\assign\Assignment;
use lflow\assign\AssignmentHandler;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;
use lflow\core\QueryServices;
use lflow\exceptions\WorkFlowException;
use lflow\lib\util\AssertHelper;

/**
 * NamedUser
 * 指定用户
 *
 * @author Mr.April
 * @since  1.0
 */
class NamedUser extends Assignment implements AssignmentHandler
{

    private static string $QUERY_SYS_USER = "SELECT id,account,real_name FROM lms_clx_system_admin";

    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {
        $actor = $ckpt->getAssignee();
        $query = app()->make(QueryServices::class);
        AssertHelper::notNull($actor, '用户[' . $actor . '不存在]');
        $where  = " WHERE delete_time is NULL";
        $where  .= " AND account in (";
        $where  .= str_repeat(" ? ,", count(explode(',', $actor)));
        $where  = substr($where, 0, strlen($where) - 1);
        $where  .= ") ";
        $result = $query->nativeQuery(self::$QUERY_SYS_USER . $where, [$actor]);
        AssertHelper::notNull((object)$result, '用户[' . $actor . '不存在或已离职]');
        return implode(',', array_column($result, 'id'));
    }
}
