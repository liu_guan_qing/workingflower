<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign\impl;


use lflow\assign\Assignment;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;

/**
 * 指定角色
 *
 * @author Mr.April
 * @since  1.0
 */
class NamedRole extends Assignment
{

    public static string $QUERY_SYS_ROLE = "SELECT user_id FROM sys_role a INNER JOIN sys_user_role b ON a.id = b.role_id and b.is_deleted = 1";

    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {

        $assing = $ckpt->getAssignee();
        AssertHelper::notNull($assing, '角色id[' . $assing . '不存在]');
        $engine = new WorkFlowEngine();
        //拼接角色id
        $where  = " WHERE a.id in (";
        $where  .= str_repeat("?,", count(explode(',', $assing)));
        $where  = substr($where, 0, strlen($where) - 1);
        $where  .= ") ";
        $result = $engine->query()->access()->queryList('', self::$QUERY_SYS_ROLE . $where, [$assing]);
        AssertHelper::notNull((object)$result, '角色id[' . $assing . '找不到对应用户]');
        return implode(',', array_column($result, 'id'));
    }
}
