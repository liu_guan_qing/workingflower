<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign\impl;



use lflow\assign\Assignment;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;
use lflow\lib\util\AssertHelper;

/**
 * 直属主管
 *
 * @author Mr.April
 * @since  1.0
 */
class DirectManager extends Assignment
{

    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {
        $order = $execution->getOrder();
        AssertHelper::notNull($order, '【异常实例不存在或被删除】');
        //通过申请人id 获或直属主管的用户id 并return  string 当前直属主管就是本人为进行逻辑编写
        return $order->getCreator();
    }

}
