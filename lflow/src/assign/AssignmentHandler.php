<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign;


use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;

/**
 * 分配参与者的处理接口
 * 建议使用Assignment接口
 */
interface AssignmentHandler
{
    /**
     * 指定用户
     */
    const NAMED_USER = 1;

    /**
     * 指定角色
     */
    const NAMED_ROLE = 2;

    /**
     * 表单field用户
     */
    const FORMS_USER = 3;

    /**
     * 直属主管
     */
    const NAMED_BOSS = 4;

    /**
     * 申请人
     */
    const PROPOSER = 5;

    /**
     * 分配类型
     */
    const ASSIGN = 'scope';

    /**
     * 自定义类处理
     */
    const ASSIGNMENT_HANDLER = 'assignment_handler';

    /**
     * 分配审核人
     */
    const APPROVE_ASSIGN = 'assignee';

    /**
     * 分配参与者方法，可获取到当前的执行对象
     *
     * @param \lflow\ckpt\TaskCkpt  $ckpt
     * @param \lflow\core\Execution $execution
     *
     * @return Object 参与者对象
     */
    public function assign(TaskCkpt $ckpt, Execution $execution): string;
}
