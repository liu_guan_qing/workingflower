<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign;

use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;

/**
 * 分配参与者抽象类
 *
 * @author Mr.April
 * @since  1.0
 */
abstract class Assignment implements AssignmentHandler
{
    /**
     * assign  分配参与者方法，可获取到当前的任务模型、执行对象
     *
     * @param \lflow\ckpt\TaskCkpt  $ckpt
     * @param \lflow\core\Execution $execution 执行对象
     *
     * @return object  参与者对象
     */
    public abstract function assign(TaskCkpt $ckpt, Execution $execution): string;
}
