<?php
/**
 *+------------------
 * Lflow
 *+------------------
 * Copyright (c) 2023~2030 gitee.com/liu_guan_qing All rights reserved.本版权不可删除，侵权必究
 *+------------------
 * Author: Mr.April(405784684@qq.com)
 *+------------------
 */
declare (strict_types=1);

namespace lflow\assign;

use lflow\assign\impl\NamedUser;
use lflow\assign\impl\DirectManager;
use lflow\assign\impl\FormsUser;
use lflow\assign\impl\NamedRole;
use lflow\assign\impl\Proposer;
use lflow\ckpt\TaskCkpt;
use lflow\core\Execution;
use lflow\exceptions\WorkFlowException;

/**
 * 分配参与者处理类
 *
 * @author Mr.April
 * @since  1.0
 */
class AssignmentTask extends Assignment
{
    public function assign(TaskCkpt $ckpt, Execution $execution): string
    {
        $scope         = (int)$ckpt->getScope();
        $assignHandler = self::ASSIGNMENT_HANDLER;
        if (isset($ckpt->$assignHandler) && !empty($ckpt->$assignHandler)) {
            //如果存在自定义类使用自定义类处理
            return (new $ckpt->$assignHandler($ckpt, $execution));
        }
        $examples = match ($scope) {
            self::NAMED_USER => app()->make(NamedUser::class),
            self::NAMED_ROLE => app()->make(NamedRole::class),
            self::FORMS_USER => app()->make(FormsUser::class),
            self::NAMED_BOSS => app()->make(DirectManager::class),
            self::PROPOSER => app()->make(Proposer::class),
            default => throw new WorkFlowException('未定义任务分配类型'),
        };
        return $examples->assign($ckpt, $execution);
    }
}
